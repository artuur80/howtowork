'use strict';

let seconds = 60*5;
let i = null;
document.addEventListener('DOMContentLoaded', init);

function init() {
    showsecond();
    document.querySelector('button').addEventListener('click', timer)
}

function timer() {
    i = setInterval(tick, 1000);

}

function tick() {
    seconds -=1;
    showsecond();

}

function showsecond(){
    let hours = Math.floor(seconds/3600);
    if (hours < 10){
        hours = '0' + hours;
    }
    let mintutes = Math.floor(seconds%3600/60);
    if (mintutes < 10){
        mintutes = '0' + mintutes;
    }
    let second = seconds%60;
    if (second < 10){
        second = '0' + second;
    }
    if (seconds === 0) {
        clearInterval(i);
        document.querySelector('button').removeEventListener('click', timer)
    }
    document.querySelector('h1').innerHTML = hours + ':' + mintutes + ':' + second;
}
